﻿Imports System.Web
Imports System.Data.Sql
Imports System.Data.SqlClient
Imports Telerik.Web.UI.DropDownListItem
Imports System.Data
Imports System.Web.Services

Partial Class About
    Inherits Page


    <WebMethod>
    Public Shared Function populateSites(ByVal mine As String) As ArrayList
        Dim list As ArrayList = New ArrayList
        Dim strConnString As String = ConfigurationManager.ConnectionStrings("FAConnectionString").ConnectionString
        Dim strQuery As String = "SELECT * FROM [STRUCTURE_ALL_CENT] WHERE [Site_TSF_Name] = @mine"
        Dim con As SqlConnection = New SqlConnection(strConnString)
        Dim cmd As SqlCommand = New SqlCommand

        cmd.CommandType = CommandType.Text
        cmd.Parameters.AddWithValue("@mine", mine)
        cmd.CommandText = strQuery
        cmd.Connection = con
        con.Open()
        Dim sdr As SqlDataReader = cmd.ExecuteReader
        While sdr.Read
            list.Add(New ListItem(sdr("Site_TSF_Name").ToString))
        End While
        con.Close()

        Return list
    End Function

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load

    End Sub
End Class