﻿
Partial Class DamBookOfLife
    Inherits System.Web.UI.Page



    Protected Sub btnBack_Click(sender As Object, e As EventArgs) Handles btnBack.Click
        Select Case Session("prevPage")
            Case "manager"
                Dim manager As String = Request.QueryString("manager")
                Response.Redirect("fat/ManagerOC.html?manager=" & manager)
            Case "customer"
                Dim customer As String = Request.QueryString("customer")
                Response.Redirect("fat/CustomerOC.html?customer=" & customer)
            Case "buoc"
                Dim bu As String = Request.QueryString("bu")
                Response.Redirect("fat/BUOC.html?bu=" & bu)
            Case "country"
                Dim continent As String = Request.QueryString("continent")
                Select Case continent
                    Case "Africa"
                        Response.Redirect("fat/CountryOC.html?extent=-20.742,-36.757,61.172,37.283&amp;zoom=true&amp;scale=true&amp;theme=light&continent=Africa")
                    Case "Australia"
                        Response.Redirect("fat/CountryOC.html?extent=110.039,-44.23,155.742,-8.603&amp;zoom=true&amp;scale=true&amp;theme=light&continent=Australia")
                    Case "South America"
                        Response.Redirect("fat/CountryOC.html?extent=-91.582,-57.622,-31.465,14.073&amp;zoom=true&amp;scale=true&amp;theme=light&continent=South America")
                    Case "Europe"
                        Response.Redirect("fat/CountryOC.html?extent=-21.27,6.119,110.566,71.124&amp;zoom=true&amp;scale=true&amp;theme=light&continent=Europe")
                End Select
        End Select
    End Sub

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        Session("prevPage") = Request.QueryString("prevPage")
        Session("siteSelected") = Request.QueryString("siteSelected")
    End Sub
End Class