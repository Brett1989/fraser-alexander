﻿<%@ Page Title="Profile" Language="VB" MasterPageFile="~/Site.master" AutoEventWireup="false" CodeFile="Profile.aspx.vb" Inherits="Profile" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">
    <h2 style="text-align: center">Admin Portal</h2>
    <hr style="height:3px">
    <div class="jumbotron">

         <telerik:RadTabStrip ID="RadTabStrip1" runat="server" Skin="Silk" SelectedIndex="0" EnableDragToReorder="True" MultiPageID="RadMultiPag1" >
            <Tabs>
                <telerik:RadTab Text="Bulk Upload"></telerik:RadTab>
                <telerik:RadTab Text="Data Editor"></telerik:RadTab>
            </Tabs>
        </telerik:RadTabStrip>
        <telerik:RadMultiPage ID="RadMultiPag1" CssClass="RadMultiPage" runat="server" SelectedIndex="0" ScrollBars="Auto">
            <telerik:RadPageView ID="RadPageView1" runat="server" Height="500" Style="overflow: hidden">
                 <div class="row">
                    <h2 style="text-align: center">Bulk Upload</h2>
                    <div class="col-md-6">                
                        <ul>
                            <li><h6>Download the Excel File.</h6></li>
                            <li><h6>Fill out the columns provided in the excel table.</h6></li>
                            <li><h6>Upload the excel spreadsheet back to the system.</h6></li>
                            <li><h6>The data will be extraced and added to the map.</h6></li>
                        </ul>
                    </div>
                    <div class="col-md-6">
                        <a href="Bulk%20Data.xlsx">Excel Data Template</a> <br />
                        <asp:Label ID="Label2" runat="server" Text="Please Select a file to upload:"></asp:Label>
                        <telerik:RadAsyncUpload ID="RadAsyncUpload1" runat="server" Skin="Silk" TemporaryFolder="~/Temp"></telerik:RadAsyncUpload>
                        <telerik:RadButton ID="btnUpload" runat="server" Text="Upload File and Add data to map" Skin="Silk"></telerik:RadButton>
                    </div>
                     <asp:Label ID="Label3" ForeColor="Green" runat="server" Text=""></asp:Label>
                </div>
            </telerik:RadPageView>
             <telerik:RadPageView ID="RadPageView2" runat="server" Height="500" Style="overflow: hidden">
                   <div class="row">
                    <h2 style="text-align: center">Data Editor</h2>
                    <hr style="height:3px">
                    <div class="col-md-12">
                         <telerik:RadFormDecorator RenderMode="Lightweight" ID="RadFormDecorator1" runat="server" DecorationZoneID="demo" EnableRoundedCorners="false" DecoratedControls="All" />
                         <telerik:RadSkinManager ID="QsfSkinManager" runat="server" Skin="Silk" />
                        <telerik:RadFormDecorator ID="QsfFromDecorator" runat="server" DecoratedControls="All" EnableRoundedCorners="false" />
                        <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server">
                        </telerik:RadAjaxLoadingPanel>
                       <telerik:RadAjaxPanel ID="RadAjaxPanel1" runat="server" LoadingPanelID="RadAjaxLoadingPanel1">
                               <telerik:RadGrid RenderMode="Lightweight" ID="RadGrid1" runat="server" DataSourceID="SqlDataSource1"
                                        AllowPaging="True" AllowAutomaticUpdates="True" Skin="Silk"
                                        AllowAutomaticDeletes="True" AllowSorting="True" CellSpacing="-1" GridLines="Both" GroupPanelPosition="Top" AutoGenerateDeleteColumn="True" AutoGenerateEditColumn="True">
                                    <MasterTableView AutoGenerateColumns="False" DataSourceID="SqlDataSource1" 
                                            DataKeyNames="OID">
                                        <Columns>
                                            <telerik:GridBoundColumn DataField="Continent" FilterControlAltText="Filter Continent column" HeaderText="Continent" SortExpression="Continent" UniqueName="Continent">
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="Country" FilterControlAltText="Filter Country column" HeaderText="Country" SortExpression="Country" UniqueName="Country">
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="Mine" FilterControlAltText="Filter Mine column" HeaderText="Mine" SortExpression="Mine" UniqueName="Mine">
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="Site_TSF_Name" FilterControlAltText="Filter Site_TSF_Name column" HeaderText="Site_TSF_Name" SortExpression="Site_TSF_Name" UniqueName="Site_TSF_Name">
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="Client" FilterControlAltText="Filter Client column" HeaderText="Client" SortExpression="Client" UniqueName="Client">
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="Business_Area_Manager" FilterControlAltText="Filter Business_Area_Manager column" HeaderText="Business_Area_Manager" SortExpression="Business_Area_Manager" UniqueName="Business_Area_Manager">
                                            </telerik:GridBoundColumn>
                                        </Columns>
                                    </MasterTableView>
                                   <ClientSettings Selecting-AllowRowSelect="true" EnableRowHoverStyle="true" />
                                </telerik:RadGrid>
                             </telerik:RadAjaxPanel>

                            <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:FAConnectionString %>" SelectCommand="SELECT * FROM [STRUCTURE_ALL_CENT]" 
                                    DeleteCommand="DELETE FROM [STRUCTURE_ALL_CENT] WHERE [OID] = @OID" 
                                    UpdateCommand="UPDATE [STRUCTURE_ALL_CENT] SET [Client] = @Client, [Site_TSF_Name] = @Site_TSF_Name, [Mine] = @Mine,[Country] = @Country, [Business_Area_Manager] = @Business_Area_Manager,[Continent] = @Continent WHERE [OID] = @OID">
                                    <DeleteParameters>
                                        <asp:Parameter Name="OID" Type="Int32" />
                                    </DeleteParameters>
                                    <UpdateParameters>
                                        <asp:Parameter Name="Site_TSF_Name" Type="String" />
                                        <asp:Parameter Name="Mine" Type="String" />
                                        <asp:Parameter Name="Client" Type="String" />
                                        <asp:Parameter Name="Country" Type="String" />
                                        <asp:Parameter Name="Business_Area_Manager" Type="String" />
                                        <asp:Parameter Name="Continent" Type="String" />
                                    </UpdateParameters>
        
                             </asp:SqlDataSource>
                    </div>
                </div>
            </telerik:RadPageView>
        </telerik:RadMultiPage>

       
        
      
    </div>
</asp:Content>

