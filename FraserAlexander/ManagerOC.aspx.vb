﻿Imports System.Web
Imports System.Data.Sql
Imports System.Data.SqlClient
Imports Telerik.Web.UI.DropDownListItem
Imports System.Data
Imports System.Web.Services

Partial Class ManagerOC
    Inherits System.Web.UI.Page

    Dim myConn As New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("FAConnectionString").ConnectionString)
    Dim myConn2 As New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("FAConnectionString2").ConnectionString)
    Private Property cmd As SqlCommand
    Dim rdr As SqlDataReader


    <WebMethod>
    Public Shared Function populateMines(ByVal manager As String) As ArrayList
        Dim list As ArrayList = New ArrayList
        Dim strConnString As String = ConfigurationManager.ConnectionStrings("FAConnectionString").ConnectionString
        Dim Query As String = "select distinct Mine from [STRUCTURE_ALL_CENT] where [Business_Area_Manager] IS NOT NULL and [Business_Area_Manager] = '" & manager & "' ORDER BY Mine"
        Dim con As SqlConnection = New SqlConnection(strConnString)
        Dim cmd As SqlCommand = New SqlCommand


        cmd.CommandType = CommandType.Text
        cmd.CommandText = Query
        cmd.Connection = con
        con.Open()
        Dim sdr As SqlDataReader = cmd.ExecuteReader
        While sdr.Read
            list.Add(New ListItem(sdr("Mine").ToString))
        End While
        con.Close()

        Return list
    End Function

    <WebMethod>
    Public Shared Function populateSites(ByVal mine As String) As ArrayList
        Dim list As ArrayList = New ArrayList
        Dim strConnString As String = ConfigurationManager.ConnectionStrings("FAConnectionString").ConnectionString
        Dim strQuery As String = "SELECT Site_TSF_Name FROM [STRUCTURE_ALL_CENT] WHERE [Mine] = '" & mine & "'"
        Dim con As SqlConnection = New SqlConnection(strConnString)
        Dim cmd As SqlCommand = New SqlCommand

        cmd.CommandType = CommandType.Text
        cmd.Parameters.AddWithValue("@mine", mine)
        cmd.CommandText = strQuery
        cmd.Connection = con
        con.Open()
        Dim rdr As SqlDataReader = cmd.ExecuteReader
        While rdr.Read
            list.Add(New ListItem(rdr("Site_TSF_Name").ToString))
        End While
        con.Close()

        Return list
    End Function

    <WebMethod>
    Public Shared Function zoomSite(ByVal site As String) As ArrayList
        Dim list As ArrayList = New ArrayList
        Dim strConnString As String = ConfigurationManager.ConnectionStrings("FAConnectionString").ConnectionString
        Dim strQuery As String = "SELECT  * FROM [STRUCTURE_ALL_CENT] WHERE Site_TSF_Name =  '@site'"
        Dim con As SqlConnection = New SqlConnection(strConnString)
        Dim cmd As SqlCommand = New SqlCommand
        Dim rdr As SqlDataReader
        Dim lat, lng As String
        Dim n_s As String
        Dim e_w As String

        cmd.CommandType = CommandType.Text
        cmd.Parameters.AddWithValue("@site", site)
        cmd.CommandText = strQuery
        cmd.Connection = con
        con.Open()
        strQuery = "SELECT * FROM [STRUCTURE_ALL_CENT] WHERE [Site_TSF_Name] = '" & site & "'"
        cmd = New SqlCommand(strQuery, con)
        rdr = cmd.ExecuteReader()
        Do While rdr.Read
            e_w = Right(rdr.Item("DDLon"), 1)
            If e_w = "W" Then
                lng = "-" & Left(rdr.Item("DDLon"), Len(rdr.Item("DDLon")) - 1)
            Else
                lng = Left(rdr.Item("DDLon"), Len(rdr.Item("DDLon")) - 1)
            End If
            n_s = Right(rdr.Item("DDLat"), 1)
            If n_s = "S" Then
                lat = "-" & Left(rdr.Item("DDLat"), Len(rdr.Item("DDLat")) - 1)
            Else
                lat = Left(rdr.Item("DDLat"), Len(rdr.Item("DDLat")) - 1)
            End If
            list.Add(New ListItem("lat", lat))
            list.Add(New ListItem("lng", lng))
            'mapframe.Src = "fat/index.html?zoom=yes&zoomtype=site&lng=" & lng & "&lat=" & lat
            Exit Do
        Loop
        con.Close()

        Return list
    End Function

    <WebMethod>
    Public Shared Function zoomSinglSite(ByVal mine As String) As ArrayList
        Dim list As ArrayList = New ArrayList
        Dim strConnString As String = ConfigurationManager.ConnectionStrings("FAConnectionString").ConnectionString
        Dim strQuery As String = "SELECT  * FROM [STRUCTURE_ALL_eCENT] WHERE Mine =  '@mine'"
        Dim con As SqlConnection = New SqlConnection(strConnString)
        Dim cmd As SqlCommand = New SqlCommand
        Dim rdr As SqlDataReader
        Dim lat, lng As String
        Dim n_s As String
        Dim e_w As String

        cmd.CommandType = CommandType.Text
        cmd.Parameters.AddWithValue("@mine", mine)
        cmd.CommandText = strQuery
        cmd.Connection = con
        con.Open()
        strQuery = "SELECT * FROM [STRUCTURE_ALL_CENT] WHERE [Mine] = '" & mine & "'"
        cmd = New SqlCommand(strQuery, con)
        rdr = cmd.ExecuteReader()
        Do While rdr.Read
            e_w = Right(rdr.Item("DDLon"), 1)
            If e_w = "W" Then
                lng = "-" & Left(rdr.Item("DDLon"), Len(rdr.Item("DDLon")) - 1)
            Else
                lng = Left(rdr.Item("DDLon"), Len(rdr.Item("DDLon")) - 1)
            End If
            n_s = Right(rdr.Item("DDLat"), 1)
            If n_s = "S" Then
                lat = "-" & Left(rdr.Item("DDLat"), Len(rdr.Item("DDLat")) - 1)
            Else
                lat = Left(rdr.Item("DDLat"), Len(rdr.Item("DDLat")) - 1)
            End If
            list.Add(New ListItem("lat", lat))
            list.Add(New ListItem("lng", lng))
            'mapframe.Src = "fat/index.html?zoom=yes&zoomtype=site&lng=" & lng & "&lat=" & lat
            Exit Do
        Loop
        con.Close()

        Return list
    End Function

    'Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
    '    Dim query As String
    '    Dim manager As String
    '    Dim lat As String
    '    Dim lng As String
    '    Dim e_w, n_s As String

    '    manager = Session("manager")
    '    Session("optionSelected") = manager
    '    query = "select distinct Mine from [STRUCTURE_ALL_CENT] where [Business_Area_Manager] IS NOT NULL and [Business_Area_Manager] = '" & manager & "' ORDER BY Mine"
    '    getList(query, "Mine", "Mine", 1)
    '    query = "select distinct Mine, DDLat, DDLon from [STRUCTURE_ALL_CENT] where [Business_Area_Manager] IS NOT NULL and [Business_Area_Manager] = '" & manager & "' ORDER BY Mine"
    '    myConn.Open()
    '    cmd = New SqlCommand(query, myConn)
    '    rdr = cmd.ExecuteReader()
    '    If rdr.HasRows Then
    '        Do While rdr.Read
    '            e_w = Right(rdr.Item("DDLon"), 1)
    '            If e_w = "W" Then
    '                lng = "-" & Left(rdr.Item("DDLon"), Len(rdr.Item("DDLon")) - 1)
    '            Else
    '                lng = Left(rdr.Item("DDLon"), Len(rdr.Item("DDLon")) - 1)
    '            End If
    '            n_s = Right(rdr.Item("DDLat"), 1)
    '            If n_s = "S" Then
    '                lat = "-" & Left(rdr.Item("DDLat"), Len(rdr.Item("DDLat")) - 1)
    '            Else
    '                lat = Left(rdr.Item("DDLat"), Len(rdr.Item("DDLat")) - 1)
    '            End If
    '            mapframe.Src = "fat/index.html?zoom=yes&zoomtype=country&lng=" & lng & "&lat=" & lat
    '            Exit Do
    '        Loop
    '    End If
    '    myConn.Close()

    'End Sub


    'Protected Sub RadButton1_Click(sender As Object, e As EventArgs) Handles RadButton1.Click
    '    Session.Clear()
    '    Response.Redirect("OperationsCatalogue.aspx")
    'End Sub

    'Protected Sub RadDropDownList1_SelectedIndexChanged(sender As Object, e As Telerik.Web.UI.DropDownListEventArgs) Handles RadDropDownList1.SelectedIndexChanged
    '    Dim query As String
    '    Dim count As Integer
    '    Dim lat, lng As String
    '    Dim e_w, n_s As String
    '    Dim singleSitename As String

    '    myConn.Open()
    '    Dim command As New SqlCommand("SELECT Count(*) from [STRUCTURE_ALL_CENT] WHERE [Mine] = '" & RadDropDownList1.SelectedValue & "'", myConn)
    '    count = CInt(command.ExecuteScalar())
    '    query = "SELECT * FROM [STRUCTURE_ALL_CENT] WHERE [Mine] = '" & RadDropDownList1.SelectedValue & "'"
    '    cmd = New SqlCommand(query, myConn)
    '    rdr = cmd.ExecuteReader()
    '    If rdr.HasRows Then
    '        Do While rdr.Read
    '            e_w = rdr.Item("SiteCoOrdsCentroid_EorW")
    '            n_s = rdr.Item("SiteCoOrdsCentroid_S_N")
    '            e_w = Right(rdr.Item("DDLon"), 1)
    '            If e_w = "W" Then
    '                lng = "-" & Left(rdr.Item("DDLon"), Len(rdr.Item("DDLon")) - 1)
    '            Else
    '                lng = Left(rdr.Item("DDLon"), Len(rdr.Item("DDLon")) - 1)
    '            End If
    '            'lng = Left(lng, InStr(lng, " ") - 1) + (Mid(lng, InStr(lng, " ") + 1, 2)) / 60 + (Right(lng, InStrRev(lng, " ") - 1)) / 3600
    '            n_s = Right(rdr.Item("DDLat"), 1)
    '            If n_s = "S" Then
    '                lat = "-" & Left(rdr.Item("DDLat"), Len(rdr.Item("DDLat")) - 1)
    '            Else
    '                lat = Left(rdr.Item("DDLat"), Len(rdr.Item("DDLat")) - 1)
    '            End If
    '            If count = 1 Then
    '                singleSitename = rdr.Item("Site_TSF_Name")
    '            End If
    '        Loop
    '        If count = 1 Then
    '            mapframe.Src = "fat/index.html?zoom=yes&zoomtype=site&lng=" & lng & "&lat=" & lat
    '            Session("siteSelected") = singleSitename
    '            btnDamBookOfLife.Visible = True
    '            lblCaption.Text = "Site Selected: "
    '        Else
    '            mapframe.Src = "fat/index.html?zoom=no"
    '            Session("mineSelected") = RadDropDownList1.SelectedValue
    '            Session("optionSelected") = RadDropDownList1.SelectedValue
    '            query = "select distinct Site_TSF_Name from [STRUCTURE_ALL_CENT] where Mine IS NOT NULL and Mine = '" & Session("mineSelected") & "' ORDER BY Site_TSF_Name"
    '            getList(query, "Site_TSF_Name", "Site_TSF_Name", 2)
    '        End If
    '    End If
    '    myConn.Close()
    'End Sub


    'Protected Sub RadDropDownList2_SelectedIndexChanged(sender As Object, e As Telerik.Web.UI.DropDownListEventArgs) Handles RadDropDownList2.SelectedIndexChanged
    '    Dim returnData As String
    '    Dim returnValue As String
    '    Dim query As String
    '    Dim lat, lng As String
    '    Dim n_s As String
    '    Dim e_w As String

    '    returnData = RadDropDownList2.DataTextField
    '    returnValue = RadDropDownList2.SelectedValue
    '    Session("optionSelected") = returnValue

    '    myConn.Open()
    '    query = "SELECT * FROM [STRUCTURE_ALL_CENT] WHERE Site_TSF_Name = '" & returnValue & "'"
    '    cmd = New SqlCommand(query, myConn)
    '    rdr = cmd.ExecuteReader()
    '    If rdr.HasRows Then
    '        Do While rdr.Read
    '            e_w = Right(rdr.Item("DDLon"), 1)
    '            If e_w = "W" Then
    '                lng = "-" & Left(rdr.Item("DDLon"), Len(rdr.Item("DDLon")) - 1)
    '            Else
    '                lng = Left(rdr.Item("DDLon"), Len(rdr.Item("DDLon")) - 1)
    '            End If
    '            'lng = Left(lng, InStr(lng, " ") - 1) + (Mid(lng, InStr(lng, " ") + 1, 2)) / 60 + (Right(lng, InStrRev(lng, " ") - 1)) / 3600
    '            n_s = Right(rdr.Item("DDLat"), 1)
    '            If n_s = "S" Then
    '                lat = "-" & Left(rdr.Item("DDLat"), Len(rdr.Item("DDLat")) - 1)
    '            Else
    '                lat = Left(rdr.Item("DDLat"), Len(rdr.Item("DDLat")) - 1)
    '            End If
    '            'lat = Left(lat, InStr(lat, " ") - 1) + (Mid(lat, InStr(lng, " ") + 1, 2)) / 60 + (Right(lat, InStrRev(lat, " ") - 1)) / 3600
    '            mapframe.Src = "fat/index.html?zoom=yes&zoomtype=site&lng=" & lng & "&lat=" & lat
    '            Exit Do
    '        Loop
    '    End If
    '    myConn.Close()

    '    Session("siteSelected") = RadDropDownList2.SelectedValue
    '    btnDamBookOfLife.Visible = True
    '    'Response.Redirect("DamBookOfLife.aspx")

    'End Sub

    'Protected Sub getList(query As String, dataField As String, dataValue As String, sql As Integer)

    '    If sql = 1 Then
    '        RadDropDownList1.Visible = True
    '        'RadDropDownList3.Visible = False
    '        SqlDataSource1.SelectCommand = query
    '        RadDropDownList1.DataTextField = dataField
    '        RadDropDownList1.DataValueField = dataValue
    '        lblCaption.Text = "Please Select a Mine: "
    '    ElseIf sql = 2 Then
    '        RadDropDownList2.Visible = True
    '        'RadDropDownList3.Visible = False
    '        SqlDataSource2.SelectCommand = query
    '        RadDropDownList2.Items.Insert(0, New Telerik.Web.UI.DropDownListItem("-"))
    '        RadDropDownList2.DataTextField = dataField
    '        RadDropDownList2.DataValueField = dataValue
    '        RadDropDownList2.SelectedIndex = -1
    '        lblCaption.Text = "Please Select a Site: "

    '    End If
    'End Sub


    'Protected Sub btnDamBookOfLife_Click(sender As Object, e As EventArgs) Handles btnDamBookOfLife.Click
    '    If Session("siteSelected") = "" Then
    '        MsgBox("Please Select a Site!")
    '        Exit Sub
    '    End If
    '    Session("prevPage") = "manager"
    '    Response.Redirect("DamBookOfLife.aspx")
    'End Sub


End Class
