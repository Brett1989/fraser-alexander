﻿<%@ Page Title="Map" Language="VB" AutoEventWireup="false" CodeFile="CustomerOC.aspx.vb" Inherits="CustomerOC" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<!DOCTYPE html>
<head runat="server">
    <meta charset="utf-8" />
    <meta http-equiv="x-ua-compatible" content="IE=11">
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title><%: Page.Title %> - Frasier Alexander</title>

    <webopt:bundlereference runat="server" path="~/Content/css" />
    <link href="~/favicon.ico" rel="shortcut icon" type="image/x-icon" />
    <script src="Scripts/jquery-2.2.0.min.js"></script>
    <script src="Scripts/bootstrap.min.js"></script>
</head>
<body >
<form runat="server">

 <script>
     $(document).ready(function () {

         $(document).scrollTop(180);        
     });
  </script>

    <telerik:RadScriptManager ID="RadScriptManager1" runat="server" ></telerik:RadScriptManager>

<div class="container body-content"> 
    Mines:<asp:DropDownList ID="ddlMines" runat="server" AppendDataBoundItems="true" onchange = "PopulateMines();">
                    <asp:ListItem Text = "Please select" Value = "0"></asp:ListItem>        
                </asp:DropDownList>
<br /><br />
    Sites:<asp:DropDownList ID="ddlSites" runat="server" onchange = "PopulateSites();">
                <asp:ListItem Text = "Please select" Value = "0"></asp:ListItem>                
            </asp:DropDownList>
<br />
    <asp:Label ID="Label3" runat="server" Text="Label"></asp:Label>
     <div class="jumbotron">
         <h2 style="text-align: center">CUSTOMER OPERATIONS CATALOGUE</h2>
        <h3 style="text-align: center"><%= Session("siteSelected") %></h3>
           <div class="row">
            <div class="col-md-9">
                <asp:Label ID="lblCaption" runat="server" Font-Bold="True" Font-Size="Small" ForeColor="Black" ></asp:Label>
                <telerik:RadDropDownList DefaultMessage="Select an item" ID="RadDropDownList1" AutoPostBack="true" runat="server" DataSourceID="SqlDataSource1" Skin="Office2010Black" LoadingPanelID="RadAjaxLoadingPanel1">
                </telerik:RadDropDownList>
                 <telerik:RadDropDownList DefaultMessage="Select an item" Visible="false"  ID="RadDropDownList2" AutoPostBack="true" runat="server" DataSourceID="SqlDataSource2" Skin="Office2010Black" LoadingPanelID="RadAjaxLoadingPanel1">
                </telerik:RadDropDownList>
                <telerik:RadButton style="line-height:23px;" height="23" ID="btnDamBookOfLife" Visible="false" runat="server" Text="Dam Book Of Life" Skin="Office2010Black" ></telerik:RadButton>                
            </div>
            <div class="col-md-3" style="text-align:right">
                <telerik:RadButton ID="RadButton1" runat="server" Text="Back" Skin="Office2010Black" ></telerik:RadButton>
            </div>
         </div>
         
        <div id="MapViewer" runat="server" style="text-align:center">
                 <iframe runat="server" id="mapframe" class="mapClass"  frameborder="0" scrolling="no" marginheight="0" marginwidth="0">
                </iframe>  
         </div>
    </div>
    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:FAConnectionString %>" >
    </asp:SqlDataSource>
     <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:FAConnectionString %>" >
    </asp:SqlDataSource>
</div>
</form>
</body>

