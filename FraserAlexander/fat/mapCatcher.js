﻿var esriMap;

function zoomTo(lat, lng, factor) {
    var point;
    point = new esri.geometry.Point(lng, lat);
    esriMap.centerAndZoom(point, factor);
}

function setZoom(faktor) {
    esriMap.setZoom(faktor);
}