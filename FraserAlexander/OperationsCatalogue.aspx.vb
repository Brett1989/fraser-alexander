﻿Imports Telerik.Web.UI

Partial Class Operations
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        Session("BU") = ""
    End Sub



    Protected Sub RadGrid1_ItemCommand(sender As Object, e As Telerik.Web.UI.GridCommandEventArgs) Handles RadGrid1.ItemCommand
        If TypeOf e.Item Is Telerik.Web.UI.GridDataItem Then
            Dim item As GridDataItem = DirectCast(e.Item, GridDataItem)
            Session("manager") = item("Business_Area_Manager").Text
            Response.Redirect("fat/ManagerOC.html?manager=" & Session("manager"))
        End If
    End Sub

    Protected Sub CustomerGrid_ItemCommand(sender As Object, e As Telerik.Web.UI.GridCommandEventArgs) Handles CustomerGrid.ItemCommand
        If TypeOf e.Item Is Telerik.Web.UI.GridDataItem Then
            Dim item As GridDataItem = DirectCast(e.Item, GridDataItem)
            Session("customer") = item("Client").Text
            Response.Redirect("fat/CustomerOC.html?customer=" & Session("customer"))
        End If
    End Sub

End Class
